# Pipeline Devops pour débuter


## Turoriels Fr

- 1.1 [Introduction & Sommaire](https://www.youtube.com/watch?v=tiSfXCM8VTw)
- 1.2 [Qu'est-ce que le devops ?](https://www.youtube.com/watch?v=yNI4-1HfiGE)
- 1.3 [Notions et Méthodes : Agile, Scrum et Fail Fast](https://www.youtube.com/watch?v=qEkL5o6PZ78)
- 1.4 [Boite à outils technique](https://www.youtube.com/watch?v=pTuUH8qugrU)
- 1.5 [Le devops et ses technologies](https://www.youtube.com/watch?v=4greqI3RC_k)
- 1.6 [Git et Giflow : comprendre](https://www.youtube.com/watch?v=ro3ouEyzFzY)
- 1.7 [Infrastructure cible](https://youtu.be/eQZkQGV_2og)
- 1.8 [Vagrant : prérequis, CLI](https://youtu.be/Q0bIwwZj0JI)
- 1.9 [Vagrant : installation de Jenkins](https://youtu.be/VLfn7F92NP8)
