#!/bin/bash

## install srvstaging

IP=$(hostname -I | awk '{print $2}')

echo "START - install srvstaging - "$IP

echo "[1]: install utils"
apt-get update -y
apt-get install -y \
    git \
    sshpass \
    wget \
    gnupg2 \
    curl \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    libnss3-tools

echo "[2]: install docker & docker-composer"
curl -fsSL https://get.docker.com | sh; >/dev/null
usermod -aG docker vagrant # authorize docker for jenkins user
curl -sL "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose 

echo "[4]: use registry without ssl"
echo "
{
 \"insecure-registries\" : [\"192.168.5.5:5000\"]
}
" >/etc/docker/daemon.json
systemctl daemon-reload
systemctl restart docker

usermod -aG docker vagrant


echo "END - install srvstaging"

